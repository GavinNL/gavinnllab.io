---
title: "Using Namecheap With Gitlab Pages"
description: "adsfasdfasdfasdf"
image: "img/categories/gitlab.jpg"
slug: ""
keywords: ""
categories: ["web"]
date: 2019-10-02T14:59:43-04:00
draft: false
---

Trying to set up a subdomain using namecheap.com to point to your custom Gitlab
Page was much more annoying that expected. Gitlab's Documentation on the subject
was quite a bit confusing when attempting to replicate it using Namecheap's DNS
management...
<!--more-->

Namecheap is the domain registrar which provides my `gavs.space` domain name.
What I wanted to do was point `http://blog.gavs.space` to
`http://gavinnl.gitlab.io`. To do that you need to do three steps:

 1. Create a Gitlab page for yourself
 2. Configure your Gitlab Page with the new domain
 3. Set up the Namecheap DNS

## Create a Gitlab Page

Creating a Gitlab Page is pretty easy. Simply create a repository under your
account called `ACCOUNTNAME.gitlab.io`. Now any code you push to that repo will
appear at `http://ACCOUNTNAME.gitlab.io`.


## Configure your Gitlab Page with the new domain.

Once you have created your Gitlab page. Go to your project's page and in the `Settings` select `Pages`.

Click `New Domain` and add your new subdomain. **Note**: in this image I have
`blog2.gavs.space` while the remaining images have `blog.gavs.space`, this is
because I took this screenshot after I had already set everything up for `blog`.
Just assume blog2 == blog.

I also disabled `Force HTTPS` since I didn't feel like buying an SSL certificate.

![Gitlab Page Domain Configuration](/img/gitlab_page.png)

Once you have set up your subdomain, you should get a screen that looks like this:


![Gitlab Page Domain Configuration](/img/gitlab.png)

## Set up the Namecheap DNS

Now in your Namecheap Advanced DNS setup, add the following lines. **Note** your
values will be different from what I have below. Your values should be pulled
from the Gitlab Pages domain setup.

| Type       | Host                                            | Value                                                            |
| ---------- | ----------------------------------------------- | ---------------------------------------------------------------- |
| A Record   | blog                                            | 35.185.44.232                                                    |
| TXT Record | blog                                            | gitlab-pages-verification-code=e081d1a324f94310c14ab68f5b99a3e86 |
| TXT Record | gitlab-pages-verification-code.blog2.gavs.space | gitlab-pages-verification-code=e081d1a324f94310c14ab68f5b99a3e86 |


The IP address `35.185.44.232` is the main IP address of the Gitlab Pages
server. Everyone's Gitlab page resides at the same IP addres. To distinguish
your page from someone else's, you will need to add the TXT record that was
generated for you. If you do not add this you will get an error when trying to
access your domain.

![Gitlab Page Domain Configuration](/img/namecheap.png)


Once you have the DNS information set up. You should be able to go back to your Gitlab Pages settings and click the `Retry Verification` button. If everything was setup correctly, your page should be verified and look like this:

![Gitlab Page Domain Configuration](/img/verified.png)

Now accessing your subdomain will point you to your hosted Gitlab page.



#### References and Credits
* [This StackOverflow question](https://stackoverflow.com/questions/49124018/gitlab-custom-domain-failed-to-verify-domain-ownership)
* [This Gitlab Issue](https://gitlab.com/gitlab-com/support-forum/issues/2427)
* Header Image by: <a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px" href="https://unsplash.com/@pankajpatel?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Pankaj Patel"><span style="display:inline-block;padding:2px 3px"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-2px;fill:white" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M10 9V0h12v9H10zm12 5h10v18H0V14h10v9h12v-9z"></path></svg></span><span style="display:inline-block;padding:2px 3px">Pankaj Patel</span></a>
