---
date : "2019-10-01T14:59:43-04:00"
title : "About me"
---

My name Gavin Wolf and I am a software engineer based out of Ottawa, Ontario. I specialize in computer graphics (Vulkan) and Linux.

I started learning C++ when I was 15 and started with Borland Turbo C++, then
moved to Mingw and finally GCC when I decided to start learning Linux in 2010.
Over the years I have dabbled in many areas of software, played around with lots
of different techs, but never put anything down on paper. This blog is a place
for me to write down the fun stuff that I work on...well, they're fun to me,
individual results may vary. Topics may include C++, Mathematics, Computer
Graphics, Linux, etc.

When I am not busy with my job and my side projects, I teach [Lindy
Hop](https://www.google.com/search?tbm=isch&sxsrf=ACYBGNRCSTom17pbxiQDAdDUwUlIjBsvBQ%3A1570109941241&source=hp&biw=2560&bih=1260&ei=9fmVXZqJDIyd5wLr7YGQCQ&q=lindy+hop&oq=&gs_l=img.1.0.35i362i39l10.0.0..6101...0.0..0.123.123.0j1......0......gws-wiz-img.....10.AhfGVX7oFLM),
a style of Swing Dancing from the 1930s and 40s, at a dance school in Ottawa.
I've been dancing since 2013 and been teaching since 2017. Although I love Lindy
Hop, this website is dedicated to tech so there won't be any posts about
dancing! Sorry to disappoint!


## Education

* B.Sc Honours Physics, Astrophysics and Photonics
* M.Sc Mathematics, Modelling and Computational Science

## Work

* Geophysical Signal Processor for [Surveying Company]
* Embedded Software Engineer for [Military Contractor]
* Visualization and Graphics Software Engineer for [Chemical Research Company]

## Interests:

C++, Vulkan, Linux, Mathematics, Simulations, Swing Dancing

